<?php
	require('../fpdf/code39.php');
	include_once('../fpdi/fpdi.php');
	include_once('clases/global.php');
	include_once('CapirestimpAfiliacionDocs.php');
	$folio 		= isset($_POST['folio']) 	? $_POST['folio'] 	: '';
	$arrpdf		= isset($_POST['rutapdf']) 	? $_POST['rutapdf'] : '';
	$impOpc		= isset($_POST['imp']) 		? $_POST['imp'] 	: '';

	$controlador = 'CtrlimpAfiliacionDocs';
	//Folio 891 ruta temporal carta derechos
	define('rutaTempCartaDerechos', "/sysx/progs/web/salida/cartaderechos/CartaDerechos.pdf");
	generarpdf($arrpdf,$folio,$impOpc);

	function generarpdf($arrPdf,$folio,$impOpc){
		
		/*******************************FOLIO 891*************************************/
		$curp = obtenerCurp($folio);
		$siefore = obtenerSiefore($curp['curp']);
		//891 Genera acuse carta derechos
		$dataAc = array("folio" => $folio, "fondorn" => $siefore['siefore']);

		$ch = curl_init();
		$url= 'http://'.$_SERVER['SERVER_ADDR'].'/impacusecartaderechos/impacusecartaderechos.php';
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
		curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($dataAc));
		$response = curl_exec($ch);
		curl_close($ch);

		$rutaFirma = '/sysx/progs/web/salida/cartaderechos/acusecartaderechosfirma_'.$folio.'.pdf';
		$rutaHuella = '/sysx/progs/web/salida/cartaderechos/acusecartaderechos_'.$folio.'.pdf';

		try{
			if(file_exists($rutaFirma))
			{
				$nomenclatura = 'acusecartaderechosfirma_'.$folio.'';
			}
			else if(file_exists($rutaHuella))
			{
				$nomenclatura = 'acusecartaderechos_'.$folio.'';
			}
	
			$im = new Imagick();
			$im->setResolution(300,300);
			$im->readimage("/sysx/progs/web/salida/cartaderechos/".$nomenclatura.".pdf"); 
			$im->setImageFormat("jpeg");
			$extension = pathinfo($nomenclatura, PATHINFO_FILENAME);
			$im->writeImage("/sysx/progs/web/salida/cartaderechos/acusecartaderechos/".$nomenclatura.".jpg"); 
			$cadena = "/sysx/progs/web/salida/cartaderechos/acusecartaderechos/".$extension.".jpg";
			$im->clear(); 
			$im->destroy();
			
			envioexp($cadena,$folio,$nomenclatura,".jpg",$curp['curp']);
		}catch(Exception $e){
			$obj->grabarlogx("FOLIO -> [$folio] Error al generar la imagen");
		}


		
		//CartaDerechos
		//SE LLAMA A BASE DE DATOS PARA CONSULTAR EL BASE64 DE LA CARTA DERECHOS CON ID 1
		$base64=obtenerBase64(1);
		//echo ("Holi: ".$base64[0]['fnobtenerbase64id']);
		$decodedImg=base64_decode($base64[0]['fnobtenerbase64id']);
		//$decodedImg=base64_decode($base64);
		file_put_contents("tempimage.jpg",$decodedImg);
		chmod("tempimage.jpg", 0777);
		$pdfNew = new FPDF();
		$pdfNew->AddPage();
		$pdfNew->Image("tempimage.jpg",0,0,212,295,'jpg');
		$pdfNew->Output(rutaTempCartaDerechos, 'F');
		unlink("tempimage.jpg");

		//$pdfdata=base64_decode($base64);
		//SE GUARDA EL PDF GENERADO CON EL BASE64 EN LA RsUTA TEMPORAL
		//$pdfdata='Hola prueba';
		//file_put_contents(rutaTempCartaDerechos, $pdfdata);
		chmod(rutaTempCartaDerechos, 0777);
		//$arrCarta = array("respuesta"=>1, "rutapdf"=> rutaTempCartaDerechos, "orden"=> 6, "mensaje"=> "Exito", "id"=> 777);
		$arrCarta = array("respuesta"=>1, "rutapdf"=> rutaTempCartaDerechos, "orden"=> 6, "mensaje"=> "Exito", "id"=> 777);
		array_push($arrPdf, $arrCarta);
		/*****************************************************************************/
		

		$arrDatos = array("respuesta"=>0, "rutapdf"=> "", "mensaje"=> "", "ruta"=> "", "nombre"=> "");
		$obj 		= new CMetodo();
		$iValPdf 	= false;
		$cont 		= sizeof($arrPdf);
		//DESCRIPCION DE COMO SE GUARDA EL PDF
		$spdfsalida	= RUTA_SALIDA."UFAF_$folio.pdf";
		$obj->grabarlogx("FOLIO -> [$folio] GENERAR PDF SALIDA --> [$spdfsalida]");
		if(file_exists($spdfsalida)){
			unlink($spdfsalida);
		}

		if($cont>0){
			$pdf = new FPDI('P','mm',array(216,279));
			foreach($arrPdf as $file){
				if($file['respuesta']==1){
					$iValPdf 	= true;
					$filename 	= $file['rutapdf'];
					$count 		= $pdf->setSourceFile($filename);
					for($i=1; $i<=$count; $i++){
						$template = $pdf->importPage($i);
						$size 	  = $pdf->getTemplateSize($template);
						$orientation = ($size['h'] > $size['w']) ? 'P' : 'L';
						if ($orientation == "P") {
							$pdf->AddPage($orientation, array($size['w'], $size['h']));
							$pdf->useTemplate($template);
						} else {
							if($impOpc == 0){
								$pdf->AddPage($orientation, array($size['h'], $size['w']));
								$pdf->useTemplate($template);
							}else{
								$imgName = RUTA_SALIDA . $folio . ".png";
								if(convertirPDfImg($filename,$imgName)){
									$pdf->AddPage('P', array($size['w'], $size['h']));
									$pdf->Image($imgName, 0, 0, 216,279);
									unlink($imgName);
								}else{
									$pdf->AddPage($orientation, array($size['h'], $size['w']));
									$obj->grabarlogx("SE PRESENTO UN PROBLEMA AL GENRAR EL FORMATO [$filename}");
								}
							}
						}
					}
				}
			}

			if($iValPdf){
				$pdf->Output($spdfsalida, 'F');
				chmod($spdfsalida, 0777);
				if(file_exists($spdfsalida)){
					$arrDatos["respuesta"] 	= 1;
					$arrDatos["rutapdf"] 	= $spdfsalida;
					$arrDatos["ruta"] 		= RUTA_SALIDA;
					$arrDatos["nombre"] 	= "UFAF_$folio.pdf";
					$obj->grabarlogx("Archivo creado con exito -> [$spdfsalida]");
				}else{
					$arrDatos["mensaje"] = "PROMOTOR: no se pudo crear el archivo.";
					$obj->grabarlogx("No se creo el documento -> [$spdfsalida]");
				}

			}else{
				$arrDatos["mensaje"] = "PROMOTOR: ninguno de los documentos esta correcto.";
				$obj->grabarlogx("GENERARPDF ERROR DE FOLIO BD -> [$folio] error documentos");
			}

		}else{
			$arrDatos["mensaje"] = "PROMOTOR: Error al crear los documentos.";
			$obj->grabarlogx("GENERARPDF ERROR DE FOLIO BD -> [$folio] arreglo -> [$cont]");
		}

		//Folio 891 Elimina el archivo temporal de la carta de derechos 891
		unlink(rutaTempCartaDerechos);
		echo json_encode($arrDatos);
	}

	function convertirPDfImg($sArchivoPFD,$imgName){
		$bresp 		= false;
		$obj 		= new CMetodo();
		$imkImagen 	= new Imagick();
		$imkImagen -> setResolution(300,300);//RESOLUCIÓN DE LA IMAGEN
		$imkImagen -> readImage($sArchivoPFD . '[0]');
		$imkImagen -> setImageFormat('png');
		//$imkImagen -> setImageBackgroundColor("white");
		//$imkImagen -> setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
		//$imkImagen -> transformImageColorspace(Imagick::COLORSPACE_GRAY);

		$imkImagen -> resizeImage(3300, 2550, Imagick::FILTER_BOX,1);
		$imkImagen -> adaptiveSharpenImage(8, 5);
		//$imkImagen -> setImageType(Imagick::IMGTYPE_BILEVEL);
		//$imkImagen -> setImageDepth(1);
		//$imkImagen -> setImageChannelDepth(Imagick::CHANNEL_ALL, 1);
		//$imkImagen -> setImageCompression(Imagick::COMPRESSION_GROUP4);
		$imkImagen -> rotateImage(new ImagickPixel(), 90);
		$imkImagen->setCompressionQuality(100);

		if($imkImagen->writeImage($imgName)){
			$obj->grabarlogx("EXITO AL GENERAR LA IMAGEN -> [$imgName] DEL PDF -> [$sArchivoPFD]");
			chmod($imgName, 0777);
			$bresp = true;
		}else{
			$obj->grabarlogx("ERR AL GENERAR LA IMAGEN -> [$imgName] DEL PDF -> [$sArchivoPFD]");
		}

		return $bresp;
	}

	//Folio 891 Obtiene carta de derechos
	function obtenerBase64($id)
	{
		$objAPI = new CapirestimpAfiliacionDocs();
		$arrApi = array(
			'id' => $id
		);
		
		try {					

			$resultAPI = $objAPI->consumirApi('obtenerBase64',$arrApi,$GLOBALS['controlador']);

			if (isset($resultAPI["estatus"]) && $resultAPI["estatus"] == 1)
			{
				foreach ($resultAPI['registros'] as $reg) {
					$arrdatos[] = array_map('utf8_encode', $reg);
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
			}
		}
		catch (Exception $e) {
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		
		return $arrdatos;
	}

	function obtenerCurp($folioCurp)
	{
		$objAPI = new CapirestimpAfiliacionDocs();
		$arrApi = array(
			'folioCurp' => $folioCurp
		);
		
		try {					

			$resultAPI = $objAPI->consumirApi('obtenerCurp',$arrApi,$GLOBALS['controlador']);

			if (isset($resultAPI["estatus"]) && $resultAPI["estatus"] == 1)
			{
				foreach ($resultAPI['registros'] as $reg)
				{
					$arrdatos['curp'] = $reg['curp'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
			}
		}
		catch (Exception $e) {
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		
		return $arrdatos;
	}

	function obtenerSiefore($sCurp)
	{
		$objAPI = new CapirestimpAfiliacionDocs();
		$arrApi = array(
			'sCurp' => $sCurp
		);
		
		try {					

			$resultAPI = $objAPI->consumirApi('obtenerSiefore',$arrApi,$GLOBALS['controlador']);

			if (isset($resultAPI["estatus"]) && $resultAPI["estatus"] == 1)
			{
				foreach ($resultAPI['registros'] as $reg)
				{
					$arrdatos['siefore'] = $reg['siefore'];
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
			}
		}
		catch (Exception $e) {
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		
		return $arrdatos;
	}

 	function envioexp($nombrearchivo,$folioafiliacion,$nomenclatura,$extension,$curp)
	{
		$objAPI = new CapirestimpAfiliacionDocs();

		$im = file_get_contents($nombrearchivo);
		$imdata = base64_encode($im);
		$fecha = date("Ymd");

		$archivo = $folioafiliacion."_".$curp."000000".$fecha."ACCD".$extension;
		
		$datos = array();

		$arrApi = array(
			'folioafiliacion' => $folioafiliacion,
			'archivo' => $archivo,
			'imdata' => $imdata
		);
		
		try {					

			$resultAPI = $objAPI->consumirApi('envioexp',$arrApi,$GLOBALS['controlador']);

			if (isset($resultAPI["estatus"]) && $resultAPI["estatus"] == 1)
			{
				foreach ($resultAPI['registros'] as $reg)
				{
					$datos = "Se Inserto Correctamente";
				}
			}
			else
			{
				if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
			}
		}
		catch (Exception $e) {
			header('HTTP/1.1 500 Internal Server Error');
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			return $mensaje;
		}
		
		return $datos;
	}
?>