<?php
include_once('clases/global.php');
include_once('CapirestimpAfiliacionDocs.php');
require('../fpdf/code39.php');
//PARA NO TENER PROBLEMAS CON LA MEMORIA, NI CON EL TIEMPO EN PHP
ini_set('memory_limit', '-1');
set_time_limit(0);
ini_set('default_socket_timeout', 10000);
ini_set("soap.wsdl_cache_enabled", FALSE);
date_default_timezone_set('America/Mazatlan');
$obj 		= new CMetodo();
$arrDatos 	= array("respuesta"=>0, "rutapdf"=> '', "mensaje"=> '');
$empleado 	= isset($_GET['empleado']) 			? $_GET['empleado'] 			: 0;
$ruta 		= isset($_GET['ruta']) 				? $_GET['ruta'] 				: RUTA_SALIDA;
$folio 		= isset($_GET['folio']) 			? $_GET['folio'] 				: 0;

$controlador = 'CtrlimpAfiliacionDocs';

if ($empleado !=  0 && $ruta != '' && $folio != 0) {/*
	$obj->grabarLogx("empleado -> [$empleado] folio -> [$folio] ruta -> [$ruta]");
	$folioImagen = consultarfoliocredencial($empleado);
	$obj->grabarLogx("Folio obtenido-> [$folioImagen]");
	if ($folioImagen != '') {
		$arrImagen 	= descargarimagen(1, $folioImagen);
		$imgTempo 	= $ruta.$folio."_TEMPO_CPRO.png";
		if ($arrImagen['respuesta'] == 1) {
			$RESP = crearArchivo($arrImagen['cdatos'], $imgTempo);
			if ($RESP == 1) {
				$nombrePDF = $ruta . $empleado . '_' . $folio . '.pdf';
				$obj->grabarLogx("Nombre del pdf a crear -> [$nombrePDF]");
				$ret = crearpdf($imgTempo, $nombrePDF);

				if($ret){
					if (file_exists($nombrePDF)) {
						chmod($nombrePDF, 0777);
						$arrDatos["respuesta"] = 1;
						$arrDatos["rutapdf"] = $nombrePDF;
						$obj->grabarLogx("Exito al crear el pdf");
					} else {
						$arrDatos["mensaje"] = "PROMOTOR: no se encontrol la credencial.";
						$obj->grabarLogx("ERROR no se puedo crear el pdf de la credencial");
					}
				}else{
					$arrDatos["mensaje"] = "PROMOTOR: no se puedo crear el documento de la credencial.";
					$obj->grabarLogx("ERROR no se puedo crear el pdf de la credencial");
				}
			} else {
				$arrDatos["mensaje"] = "PROMOTOR: no se puedo crear la imagen de la credencial.";
				$obj->grabarLogx("ERROR no se puedo crear la imagen de la credencial respuesta-> [$RESP]");
			}
		} else {
			$arrDatos["mensaje"] = "PROMOTOR: no se encontro imagen de la credencial.";
			$obj->grabarLogx("ERROR imagen no encontrada con el folio-> [$folioImagen] respuesta-> [" . $arrImagen['respuesta'] . "]");
		}
	} else {
		$arrDatos["mensaje"] = "PROMOTOR: no se encontro informacion.";
		$obj->grabarLogx("ERROR folio no encontrado-> [$folioImagen]");
	}
} else {
	$arrDatos["mensaje"] = "PROMOTOR: error al obtener la credencial del promotor.";
	$obj->grabarLogx("ERROR en parametros => empleado-> [$empleado] folio-> [$folio] ruta-> [$ruta]");*/
}

echo json_encode($arrDatos);

function consultarfoliocredencial($empleado)
{
	$folioImagen 	= "";
	$objGn = new CMetodo();
	$objAPI = new CapirestimpAfiliacionDocs();
	$arrApi = array(
		'empleado' => $empleado
	);
	
	try {					

		$objGn->grabarLogx("Inicio API Rest consultarfoliocredencial");

		$resultAPI = $objAPI->consumirApi('consultarfoliocredencial',$arrApi,$GLOBALS['controlador']);

		if (isset($resultAPI["estatus"]) && $resultAPI["estatus"] == 1)
		{
			foreach ($resultAPI['registros'] as $reg) {
				$folioImagen = $reg['respuesta'];
			}
		}
		else
		{
			if(isset($resultAPI["tipo"]))
				header('HTTP/1.1 401 Unauthorized');
		}
	}
	catch (Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}
	
	return $folioImagen;
}

function descargarimagen($opcion, $folioImagen)
{
	$datosImagen	= array();
	$objGn = new CMetodo();
	$objAPI = new CapirestimpAfiliacionDocs();
	$arrApi = array(
		'opcion' => $opcion,
		'folioImagen' => $folioImagen
	);
	
	try {
		
		$objGn->grabarLogx("Inicio API Rest descargarimagen");

		$resultAPI = $objAPI->consumirApi('descargarimagen',$arrApi,$GLOBALS['controlador']);

		if (isset($resultAPI["estatus"]) && $resultAPI["estatus"] == 1) {

			foreach ($resultAPI["registros"] as $reg) {
				$datosImagen['respuesta'] = $reg['irespuesta'];
				$datosImagen['rimagen'] = $reg['rimagen'];
				$datosImagen['cdatos'] = $reg['cdatos'];
			}
			$objGn->grabarLogx('respuesta [fndescargarimagen] imagen -> ' . $datosImagen['rimagen'] = $reg['rimagen'] . ' ,respuesta -> ' . $datosImagen['respuesta']);

			if ($datosImagen['respuesta'] == 0 && $opcion == 1) {
				$objGn->grabarLogx('no se encontro imagen en la base de datos->' . IPIMAGENES);
				$datosImagen = descargarimagen(2, $folioImagen);
			}
		} else {

			if(isset($resultAPI["tipo"]))
					header('HTTP/1.1 401 Unauthorized');
		}
		
	} catch (Exception $e) {

		header('HTTP/1.1 500 Internal Server Error');
		$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		return $mensaje;
	}

	return $datosImagen;
}

function crearArchivo($archivo, $cnombre){
	$archivo 	= str_replace('data:image/tiff;base64,', '', $archivo);
	$archivo 	= str_replace(' ', '+', $archivo);
	$data 		= base64_decode($archivo);
	$success 	= file_put_contents($cnombre, $data);
	$respuesta 	= $success ? 1 : 0;
	chmod($cnombre, 0777);
	if($respuesta == 1){
		$image = new Imagick($cnombre);
		$image->setImageFormat('png');
		$image->writeImage($cnombre);
		chmod($cnombre,"0777");
	}
	return $respuesta;
}

function crearpdf($rutaImagent, $pdfGenerado){
	$pdf            = new PDF_Code39();
	$iResp 			= 0;
	$iAnchoHojaMM   = 279;
	$iAltoHojaMM    = 215;
	$pdf->SetAutoPageBreak(true, 0);
	$pdf->AliasNbPages();
	$pdf->AddPage('P', 'Letter');
	$pdf->SetFont('Arial', '', 8);
	$pdf->SetTextColor(0, 0, 0);

	if (file_exists($rutaImagent)) {
		$pdf->Image($rutaImagent, 0, 0, $iAltoHojaMM, $iAnchoHojaMM);
		unlink($rutaImagent);
		$pdf->Output($pdfGenerado, 'F');
		$iResp = 1;
	}
	return $iResp;
}