<?php
    $configaraciones    = simplexml_load_file("../conf/webconfig.xml");
    //BASE DE DATOS DE AFOREGLOBAL
    $ipServidor         = $configaraciones->Spa;
    $sUsuario           = $configaraciones->Usuario;
    $sBasedeDatos       = $configaraciones->Basededatos;
    $sPassword          = $configaraciones->Password;
    //BASE DE DATOS DE IMAGENES TEMPORAL
    $IpImagenes         = $configaraciones->IpImagenes;
    $UsuarioImg         = $configaraciones->UsuarioImg;
    $BasededatosImg     = $configaraciones->BasededatosImg;
    $PasswordImg        = $configaraciones->PasswordImg;
    //BASE DE DATOS DE IMAGENES HISTORICO
    $IpServidorHistorico = $configaraciones->IpServidorHistorico;
    //VARIABLES GLOBALES
    define('RUTA_LOGX',             '/sysx/progs/afore/log/impafiliaciondocs');
    define('RUTA_SALIDA',           '/sysx/progs/web/salida/impresiondocumentosafiliacion/');
    //VARAIBLES DE CONEXCION
    define("IPSERVIDOR",            $ipServidor);
    define("BASEDEDATOS",           $sBasedeDatos);
    define("USUARIO",               $sUsuario);
    define("PASSWORD",              $sPassword);
    define("IPIMAGENES",            $IpImagenes);
    define("USUARIOIMG",            $UsuarioImg );
    define("BASEDEDATOSIMG",        $BasededatosImg);
    define("PASSWORDIMG",           $PasswordImg);
    define("IPSERVIDORHISTORICO",   $IpServidorHistorico);

    class CMetodo{
        function getIpRemoto(){
            if (!empty($_SERVER['HTTP_CLIENT_IP']))
                return $_SERVER['HTTP_CLIENT_IP'];

            if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];

            return $_SERVER['REMOTE_ADDR'];
        }

        function grabarLogx($cadLogx){
            $cIpCliente = CMetodo::getIpRemoto();
            $rutaLog =  RUTA_LOGX .  '-' . date("d") . '-' . date("m") . '-' . date("Y") . ".txt";
            $cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
            $file = fopen($rutaLog, "a");
            if ($file) {
                fwrite($file, $cad);
            }
            fclose($file);
        }
    }
?>

